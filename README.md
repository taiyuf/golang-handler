# golang-handler

## Initialize

// Logger is the interface for logger.
type Logger interface {
	Info(...interface{})
	Debug(...interface{})
	Error(...interface{})
}

```
logger := ...

h := NewHandler(logger, PathOfAccessLog)
h.Debug = true // default: false

```

## Usage

### HealthCheckFunc

### NotFoundFunc

### CheckApikey

### ResponseStatusOk

### ResponseNotFoundError

### ResponseInvalidError

### ResponseInternalServerError

### ResponseJSON

### WriteAccessLog

### GetIPAdress
