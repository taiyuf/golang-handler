package handler

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"html"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

// Handler is the struct for the hander.
type Handler struct {
	Log     Logger
	LogPath string
	Debug   bool
}

// Access_log is the struct for access.log.
type AccessLog struct {
	RemoteAddr string `json:"remote_addr"`
	RemoteUser string `json:"remote_user"`
	Time       string `json:"time"`
	Request    string `json:"request"`
	Status     int    `json:"status"`
	BodyBySent int    `json:"body_by_sent"`
	UserAgent  string `json:"user_agent"`
	LiveID     string `json:"live_id"`
	Referer    string `json:"referer"`
}

// Logger is the interface for logger.
type Logger interface {
	Info(...interface{})
	Debug(...interface{})
	Error(...interface{})
}

// NewHandler is initialize of the handler struct.
func NewHandler(l Logger, path string) *Handler {
	return &Handler{
		Log:     l,
		LogPath: path,
		Debug:   false,
	}
}

// HealthCheckFunc is for the health check request.
func (h *Handler) HealthCheckFunc(w http.ResponseWriter, r *http.Request) {
	h.ResponseStatusOk(w, r, "OK")
}

// NotFoundFunc return the 404 message.
func (h *Handler) NotFoundFunc(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusNotFound)
	msg := fmt.Sprintf("*** 404 Not found: %v, %v", r.Method, html.EscapeString(r.URL.Path))
	_, _ = fmt.Fprintf(w, msg+"\n")

	if os.Getenv("MODE") == "test" {
		return
	}

	h.WriteAccessLog(r, http.StatusNotFound, msg, "")
}

// CheckApikey check the apikey.
func (h *Handler) CheckApikey(r *http.Request, header string, keys []string) bool {

	// 大文字、小文字を無視して、ヘッダーをマッチする
	headers := make(map[string]string, len(r.Header))

	for k := range r.Header {
		headers[strings.ToLower(k)] = r.Header.Get(k)
	}

	key := r.Header.Get(strings.ToLower(header))

	for _, v := range keys {
		if v == "" {
			continue
		}

		if v == key {
			h.Log.Debug("apikey matched: " + key)
			return true
		}
	}

	return false
}

// ResponseStatusOk return the string with status 200.
func (h *Handler) ResponseStatusOk(w http.ResponseWriter, r *http.Request, str string) {
	code := http.StatusOK
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(code)
	fmt.Fprintf(w, str+"\n")

	if os.Getenv("MODE") == "test" {
		return
	}

	h.WriteAccessLog(r, code, str, "")
}

// ResponseInvalidError return the string with status 40x.
func (h *Handler) ResponseInvalidError(w http.ResponseWriter, r *http.Request, str string, code int) {
	if code == 0 {
		code = http.StatusBadRequest
	}
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(code)
	fmt.Fprintf(w, str+"\n")

	if os.Getenv("MODE") == "test" {
		return
	}

	h.WriteAccessLog(r, code, str, "")
}

// ResponseNotFoundError return the string with status 40x.
func (h *Handler) ResponseNotFoundError(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusNotFound)
	msg := fmt.Sprintf("*** 404 Not found: %v, %v", r.Method, html.EscapeString(r.URL.Path))

	fmt.Fprintf(w, msg+"\n")

	if os.Getenv("MODE") == "test" {
		return
	}

	h.WriteAccessLog(r, http.StatusNotFound, msg, "")
}

// ResponseInternalServerError return the  string withg status 500.
func (h *Handler) ResponseInternalServerError(w http.ResponseWriter, r *http.Request, str string) {
	w.Header().Set("Content-Type", "text/plain")
	code := http.StatusInternalServerError
	w.WriteHeader(code)
	fmt.Fprintf(w, str+"\n")

	if os.Getenv("MODE") == "test" {
		return
	}

	h.WriteAccessLog(r, code, str, "")
}

// ResponseJSON return the json with status 200.
func (h *Handler) ResponseJSON(w http.ResponseWriter, r *http.Request, st interface{}) {
	b, err := json.Marshal(st)
	if err != nil {
		msg := fmt.Sprintf("*** fail to encode json: %v", st)
		h.ResponseInternalServerError(w, r, msg)
		return
	}

	code := http.StatusOK
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	fmt.Fprintf(w, string(b)+"\n")
	h.Log.Debug(string(b) + "\n")

	if os.Getenv("MODE") == "test" {
		return
	}

	h.WriteAccessLog(r, code, string(b)+"\n", "")
}

func (h *Handler) checkExistsParam(w http.ResponseWriter, r *http.Request, param string) bool {
	if r.FormValue(param) == "" {
		msg := fmt.Sprintf("*** the parameter: %v is not found.", param)
		h.Log.Error(msg)
		code := http.StatusBadRequest
		h.ResponseInvalidError(w, r, msg, code)
		return false
	}

	return true
}

// WriteAccessLog write the access log by JSON.
func (h *Handler) WriteAccessLog(r *http.Request, status int, content, user string) {

	if h.LogPath == "" {
		return
	}

	ac, err := os.OpenFile(h.LogPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		h.Log.Error(fmt.Sprintf("*** Could not write the access log: %v", h.LogPath))
	}
	defer ac.Close()

	t := time.Now().Local()
	ip := r.RemoteAddr
	ff := GetIPAdress(r)
	if ff != "" {
		ip = ff
	}

	al := &AccessLog{
		RemoteAddr: ip,
		Request:    r.RequestURI,
		Status:     status,
		BodyBySent: len([]byte(content)),
		Time:       t.Format("2006/01/02 15:04:05"),
		UserAgent:  r.UserAgent(),
		Referer:    r.Referer(),
		RemoteUser: user,
	}

	b, jerr := json.Marshal(al)
	if jerr != nil {
		h.Log.Error(fmt.Sprintf("*** Could not parse to json: %v", jerr))
	}

	// Ctx.Dump("json", string(b))

	writer := bufio.NewWriter(ac)
	_, werr := writer.WriteString(string(b) + "\n")
	if werr != nil {
		h.Log.Error(fmt.Sprintf("*** Could not write to access log: %v", werr))
	}

	writer.Flush()
}

// ipRange - a structure that holds the start and end of a range of ip addresses
type ipRange struct {
	start net.IP
	end   net.IP
}

// inRange - check to see if a given ip address is within a range given
func inRange(r ipRange, ipAddress net.IP) bool {
	// strcmp type byte comparison
	if bytes.Compare(ipAddress, r.start) >= 0 && bytes.Compare(ipAddress, r.end) < 0 {
		return true
	}
	return false
}

var privateRanges = []ipRange{
	ipRange{
		start: net.ParseIP("10.0.0.0"),
		end:   net.ParseIP("10.255.255.255"),
	},
	ipRange{
		start: net.ParseIP("100.64.0.0"),
		end:   net.ParseIP("100.127.255.255"),
	},
	ipRange{
		start: net.ParseIP("172.16.0.0"),
		end:   net.ParseIP("172.31.255.255"),
	},
	ipRange{
		start: net.ParseIP("192.0.0.0"),
		end:   net.ParseIP("192.0.0.255"),
	},
	ipRange{
		start: net.ParseIP("192.168.0.0"),
		end:   net.ParseIP("192.168.255.255"),
	},
	ipRange{
		start: net.ParseIP("198.18.0.0"),
		end:   net.ParseIP("198.19.255.255"),
	},
}

// isPrivateSubnet - check to see if this ip is in a private subnet
func isPrivateSubnet(ipAddress net.IP) bool {
	// my use case is only concerned with ipv4 atm
	if ipCheck := ipAddress.To4(); ipCheck != nil {
		// iterate over all our ranges
		for _, r := range privateRanges {
			// check if this ip is in a private range
			if inRange(r, ipAddress) {
				return true
			}
		}
	}
	return false
}

// GetIPAdress return the IP Address of request.
func GetIPAdress(r *http.Request) string {
	ip := r.RemoteAddr
	var hip string

	for _, h := range []string{"X-Forwarded-For", "X-Real-Ip"} {
		addresses := strings.Split(r.Header.Get(h), ",")
		// march from right to left until we get a public address
		// that will be the address right before our proxy.
		for i := len(addresses) - 1; i >= 0; i-- {
			tmp := strings.TrimSpace(addresses[i])
			// header can contain spaces too, strip those out.
			realIP := net.ParseIP(ip)
			// if !realIP.IsGlobalUnicast() || isPrivateSubnet(realIP) {
			if !realIP.IsGlobalUnicast() {
				// bad address, go to next
				hip = tmp
				continue
			}
		}
	}

	if hip != "" {
		return hip
	}

	return ip
}
